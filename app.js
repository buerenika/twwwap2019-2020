


// Třída shop, reprezentuje shop
class Shop {
    constructor(id, nazev, vyrobce, mnozstvi, obchod) {
        this.id = id;
        this.nazev = nazev;
        this.vyrobce = vyrobce;
        this.mnozstvi = mnozstvi;
        this.obchod = obchod;
    }
}



// Třída UI, stará se o UI úkoly
class UI {
    static displayShops() {
        const list = document.querySelector('#shop-list');
        
        while (list.firstChild) {
            list.firstChild.remove();
        }

        const shops = Store.getShops();
        shops.sort(function(a, b){
            if(a.nazev < b.nazev) { return -1; }
            if(a.nazev > b.nazev) { return 1; }
            return 0;
        })
        
        shops.forEach((shop) => UI.addShopToList(list, shop));
    }

    static myFunction() {
        const x = document.getElementById("myTopnav");
        if (x.className === "topnav") {
            x.className += " responsive";
        } else {
            x.className = "topnav";
        }
    }

    static displaySelectedShops(shopName) {
        const list = document.querySelector('#shopOb-list');
        
        while (list.firstChild) {
            list.firstChild.remove();
        }

        const shops = Store.getSelectedShops(shopName);
        shops.sort(function(a, b){
            if(a.nazev < b.nazev) { return -1; }
            if(a.nazev > b.nazev) { return 1; }
            return 0;
        })
        
        shops.forEach((shop) => UI.addShopToList2(list, shop));
    }

    //Přidání položky do listu
    static addShopToList(list, shop) {
        const row = document.createElement('tr');

        row.innerHTML = `
            <td>${shop.id}</td>
            <td>${shop.nazev}</td>
            <td>${shop.vyrobce}</td>
            <td>${shop.mnozstvi}</td>
            <td>${shop.obchod}</td>
            <td><a href="#" class="btn btn-danger btn-sm delete" onclick="Store.confirmDelete()">X</a></td>
        `;

        list.appendChild(row);
    }

    //Přidání položky do listu
    static addShopToList2(list, shop) {
        const row = document.createElement('tr');

        row.innerHTML = `
            <td>${shop.id}</td>
            <td>${shop.nazev}</td>
            <td>${shop.vyrobce}</td>
            <td>${shop.mnozstvi}</td>
            <td>${shop.obchod}</td>
            <td><a href="#" class="btn btn-danger btn-sm delete" onclick="Store.confirmDelete2()">X</a></td>
        `;

        list.appendChild(row);
    }

    //Odebrání položky
    static deleteShop(el) {
        if(el.classList.contains('delete')) {
            el.parentElement.parentElement.remove();
        }
    }

    // Hlášky - místo, kde se budou zobrazovat - Položka přidána, doplňte (nad formulářem)
    static showAlert(message, className) {
        const div = document.createElement('div');
        div.className = `alert alert-${className}`;
        div.appendChild(document.createTextNode(message));
        const messenger = document.querySelector('.messenger');
        const form = document.querySelector('#shop-form');
        messenger.insertBefore(div, form);

        // Zmizení hlášky po 3 sekundách
        setTimeout(() => document.querySelector('.alert').remove(), 3000);
    }

    // Hláška - místo, kde se budou zobrazovat -> Položka odstraněna (pod výpisem)
    static showAlertDelete(message, className) {
        const div = document.createElement('div');
        div.className = `alert alert-${className}`;
        div.appendChild(document.createTextNode(message));
        const containers = document.querySelector('.containers');
        const table = document.querySelector('#tables');
        containers.insertBefore(div, table);

        // Zmizení hlášky po 4 sekundách
        setTimeout(() => document.querySelector('.alert').remove(), 4000);
    }

    // Hláška - místo, kde se budou zobrazovat -> Položka odstraněna (pod výpisem)
    static showAlertDelete2(message, className) {
        const div = document.createElement('div');
        div.className = `alert alert-${className}`;
        div.appendChild(document.createTextNode(message));
        const messengerOb = document.querySelector('.messengerOb');
        const table = document.querySelector('#tables2');
        messengerOb.insertBefore(div, table);

        // Zmizení hlášky po 4 sekundách
        setTimeout(() => document.querySelector('.alert').remove(), 4000);
    }

    // vyčistí pole po přidání položky do seznamu
    static clearFields() {        
        document.getElementById("shop-form").reset();
    }
}



// Třída Store, stará se o úložiště
class Store {
    static getShops() {
        let shops;
        if(localStorage.getItem('shops') === null) {
            shops = [];
        } else {
            shops = JSON.parse(localStorage.getItem('shops'));
        }

        return shops;
    }
    
    static getSelectedShops(shopName) {
        let shops = Store.getShops();
        
        let ret = [];
        shops.forEach((shop, index) => {
            if(shop.obchod === shopName) {
                ret.push(shop);
                //console.log(shop);
            }
        });

        return ret;
    }

    //Přidání položky a seřezení podle abecedy
    static addShop(shop) {
        const shops = Store.getShops();
        shops.push(shop);

        localStorage.setItem("shops", JSON.stringify(shops));
    }

    static getNewID() {
        let lastID;
        if(localStorage.getItem('lastID') === null) {
            lastID = 1;
            localStorage.setItem("lastID", lastID);
        } else {
            lastID = Number(localStorage.getItem('lastID')) + 1;
            localStorage.setItem("lastID", lastID);
        }
        
        return lastID;    
    }

    //Odebrání položky
    static removeShop(id) {
        console.log(id);
        const shops = Store.getShops();
        shops.forEach((shop, index) => {
            if(Number(shop.id) === Number(id)) {
                shops.splice(index, 1);
                //console.log(shops)
            }
        });

        localStorage.setItem('shops', JSON.stringify(shops));
    }

    // Odebere položku z paměti, pouze pokud odsouhlasíme confirm + vypíše hlášky
    static confirmDelete(){
        if (confirm('Opravdu chcete položku odebrat?') == true) {
            document.querySelector('#shop-list').addEventListener('click', (e) => {
     
                UI.deleteShop(e.target);
                Store.removeShop(e.target.parentElement.parentNode.firstElementChild.innerHTML);
                UI.showAlertDelete('Položka byla úspěšně odebrána');
            }); 
            
        } else {
            UI.showAlertDelete('Odebrání položky bylo zrušeno');
        }
    }

    static confirmDelete2(){
        if (confirm('Opravdu chcete položku odebrat?') == true) {
            document.querySelector('#shopOb-list').addEventListener('click', (e) => {
     
                UI.deleteShop(e.target);
                Store.removeShop(e.target.parentElement.parentNode.firstElementChild.innerHTML);
                UI.showAlertDelete('Položka byla úspěšně odebrána');
                //Refresh UI
                UI.displayShops()
            }); 
            
        } else {
            UI.showAlertDelete2('Odebrání položky bylo zrušeno');
        }
    }
}



// Event: zobrazení položek
document.addEventListener('DOMContentLoaded', UI.displayShops);

// Event: Přidání položky
document.querySelector('#shop-form').addEventListener('submit', (e) => {

    // Zabraňuje skutečnému odeslání 
    e.preventDefault();

    // Co získává z hodnot
    let nazev = document.querySelector('#nazev').value;
    let vyrobce = document.querySelector('#vyrobce').value;
    let mnozstvi = document.querySelector('#mnozstvi').value;
    let obchod = Array.from(document.getElementsByName("obchod")).find(r => r.checked).value;

    // Ověření + hláška
    if(nazev === '' || vyrobce === '' || mnozstvi === '' || obchod === '') {
        UI.showAlert('Prosím vyplňte všechny pole', 'danger');
    } else {
        // Nový objekt položka
        const shop = new Shop(Store.getNewID(), nazev, vyrobce, mnozstvi, obchod);

        // Přidání položky do paměti
        Store.addShop(shop)
        
        //Refresh UI
        UI.displayShops()

        // Hláška
        UI.showAlert('Položka byla úspěšně přidaná', 'success');

        // Vyčištění polí
        UI.clearFields();
    }
});



// Vypíše seznam dle obchodu po kliknutí na type radio
document.querySelector('#shopOb-Form').addEventListener('click', (e) => {

    let obchod = Array.from(document.getElementsByName("obchodOb")).find(r => r.checked).value;
    //console.log(obchod);
    
    UI.displaySelectedShops(obchod);

});


